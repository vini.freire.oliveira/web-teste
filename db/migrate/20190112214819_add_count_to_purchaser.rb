class AddCountToPurchaser < ActiveRecord::Migration[5.2]
  def change
    add_column :purchasers, :count, :string
  end
end
