require 'rails_helper'

RSpec.describe SaleController, type: :controller do
  
  describe "GET #index" do
    it "returns http found" do
      get :index
      expect(response).to have_http_status(:found)
    end
  end

  describe "Post #create" do
    it "returns http found" do
      post :create
      expect(response).to have_http_status(:found)
    end
  end

end
