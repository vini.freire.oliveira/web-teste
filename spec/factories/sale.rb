FactoryBot.define do
  
    factory :sale do    
        purchaser_id { create(:purchaser).id}
        merchant_id { create(:merchant).id}
        item_id { create(:item).id}
    end

end
  