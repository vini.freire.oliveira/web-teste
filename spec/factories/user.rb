FactoryBot.define do
    factory :user do
        email {  FFaker::Internet.unique.email}
        password {  "secret123" }
        password_confirmation {  "secret123" }
    end
end
