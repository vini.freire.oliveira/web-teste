FactoryBot.define do

    factory :item do
        description { FFaker::Lorem.unique.phrase }
        price { FFaker::Random.rand(0.0..10000.0).round(3) }
    end

end
