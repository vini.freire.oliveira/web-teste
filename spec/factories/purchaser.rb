FactoryBot.define do

    factory :purchaser do
        name { FFaker::Name.unique.name }
        count { FFaker::Random.rand(1..100) }
    end

end
  