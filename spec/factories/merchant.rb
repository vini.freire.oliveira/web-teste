FactoryBot.define do

    factory :merchant do
        name { FFaker::Name.unique.name }
        address { FFaker::Address.unique.city }
    end

end
  