require 'rails_helper'

RSpec.describe Sale, type: :model do

  describe 'Associations' do
    it { should belong_to :purchaser }
    it { should belong_to :merchant }
    it { should belong_to :item }
  end

end
