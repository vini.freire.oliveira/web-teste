require 'rails_helper'

RSpec.describe Purchaser, type: :model do
  before(:each) do
    @purchaser = FactoryBot.create(:purchaser)
  end


  describe 'Associations' do
    it { should have_many :sales }
  end
  
  describe 'Validations' do
    context 'name' do
      it 'should not be empty' do
          @purchaser.name = nil
          expect(@purchaser).to_not be_valid
      end

      it 'should be unique' do
        purchaser_repeated = FactoryBot.create(:purchaser)
        purchaser_repeated.name = @purchaser.name
        expect(purchaser_repeated).to_not be_valid
      end
    end

    context 'count' do
      it 'should not be empty' do
          @purchaser.count = nil
          expect(@purchaser).to_not be_valid
      end
    end
    
  end 
end
