require 'rails_helper'

RSpec.describe Item, type: :model do
  
  before(:each) do
    @item = FactoryBot.create(:item)
  end

  describe 'Associations' do
    it { should have_many :sales }
  end

  describe 'Validations' do
    context 'price' do
        it 'should not be empty' do
            @item.price = nil
            expect(@item).to_not be_valid
        end
    end

    context 'description' do
      it 'should not be empty' do
          @item.description = nil
          expect(@item).to_not be_valid
      end
    end
    
  end 


end
