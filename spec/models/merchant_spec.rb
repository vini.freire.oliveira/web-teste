require 'rails_helper'

RSpec.describe Merchant, type: :model do

  before(:each) do
    @merchant = FactoryBot.create(:merchant)
  end


  describe 'Associations' do
    it { should have_many :sales }
  end

  describe 'Validations' do
    context 'name' do
      it 'should not be empty' do
          @merchant.name = nil
          expect(@merchant).to_not be_valid
      end

      it 'should be unique' do
        merchant_repeated = FactoryBot.create(:merchant)
        merchant_repeated.name = @merchant.name
        expect(merchant_repeated).to_not be_valid
      end
    end

    context 'address' do
      it 'should not be empty' do
          @merchant.address = nil
          expect(@merchant).to_not be_valid
      end
    end
    
  end 

end
