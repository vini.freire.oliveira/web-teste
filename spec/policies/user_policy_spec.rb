require 'rails_helper'

RSpec.describe UserPolicy, type: :policy do
  before(:each) do
    @admin = FactoryBot.create(:user)
    @admin.user_type = :admin
    @ordinary = FactoryBot.create(:user)
  end

  let(:user) { FactoryBot.create(:user) }
  let(:ordinary) { UserPolicy.new(@ordinary, user) }
  let(:admin) { UserPolicy.new(@admin, user) }

  permissions :index? do
    context "User show all sales" do
      it "grants access if user is an ordinary" do
        expect(ordinary).to permitted_to(:index?)
      end
  
      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:index?)
      end
    end
  end

  permissions :show? do
    context "User show his sales" do
      it "grants access if user is an ordinary" do
        expect(ordinary).to permitted_to(:show?)
      end

      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:show?)
      end

    end
  end

  permissions :upload? do
    context "User upload sales" do
      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:upload?)
      end

      it "denies access if user is an ordinary" do
        expect(ordinary).not_to permitted_to(:upload?)
      end
    end
    
  end

  permissions :create? do
    context "User create sales" do
      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:create?)
      end

      it "denies access if user is an ordinary" do
        expect(ordinary).not_to permitted_to(:create?)
      end
    end
    
  end

  permissions :destroy? do
    context "User delete sales" do
      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:destroy?)
      end

      it "denies access if user is an ordinary" do
        expect(ordinary).not_to permitted_to(:destroy?)
      end
    end
    
  end


end
