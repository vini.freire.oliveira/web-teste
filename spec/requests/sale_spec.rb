require 'rails_helper'
 
RSpec.describe "Sale", type: :request do

  describe "GET /index" do
    context "With Invalid authentication " do
      it "returns 401" do
        expect(response).to be_nil
      end
    end

    context "With Valid authentication headers and authorization" do
      before do
        @user = create(:user)
        sign_in @user
        @user.user_type = :admin
        get "/sale"
      end
 
      it "returns 200" do
        expect(response.status).to eql(200)
      end

    end

  end

  describe "POST /upload" do
    
    context "With Invalid authentication " do
      it "returns 401" do
        expect(response).to be_nil
      end
    end

    context "With Valid authentication and without authorization" do
      before do
        @user = create(:user)
        sign_in @user
        @user.user_type = :ordinary
        sale = FactoryBot.create(:sale)
        file = File.read("public/example_input.tab")
        post "/sale/#{sale.id}/upload", params: {sale: attributes_for(:sale).merge({"tab": file})}
      end
 
      it "returns 401" do
        expect(response.status).to eql(401)
      end

    end

    context "With Valid authentication headers and authorization" do
      before do
        @user = create(:user)
        sign_in @user
        @user.user_type = :admin
        sale = FactoryBot.create(:sale)
        file = File.read("public/example_input.tab")
        post "/sale/#{sale.id}/upload", params: {sale: attributes_for(:sale).merge({"tab": file})}
      end
 
      it "returns 200" do
        expect(response.status).to eql(302)
      end

    end

  end


  describe "POST /create" do
    
    context "With Invalid authentication " do
      it "returns 401" do
        expect(response).to be_nil
      end
    end

    context "With Valid authentication and without authorization" do
      before do
        @user = create(:user)
        sign_in @user
        @user.user_type = :ordinary
        post "/sale", params: {sale: attributes_for(:sale)}
      end
 
      it "returns 401" do
        expect(response.status).to eql(401)
      end

    end

  end

  describe "Delete /delete" do
    
    context "With Invalid authentication " do
      it "returns 401" do
        expect(response).to be_nil
      end
    end

    context "With Valid authentication and without authorization" do
      before do
        @user = create(:user)
        sign_in @user
        @user.user_type = :ordinary
        sale = FactoryBot.create(:sale)
        delete "/sale/#{sale.id}"
      end
 
      it "returns 401" do
        expect(response.status).to eql(401)
      end

    end

    context "With Valid authentication headers and authorization" do
      before do
        @user = create(:user)
        sign_in @user
        @user.user_type = :admin
        sale = FactoryBot.create(:sale)
        delete "/sale/#{sale.id}"
      end
 
      it "returns 204" do
        expect(response.status).to eql(204)
      end

    end

  end
 
  
 
end
