require 'rails_helper'

RSpec.describe "Sessions" do

    it "sign user in" do
        user = FactoryBot.create(:user)

        sign_in user
        get authenticated_root_path
        expect(controller.current_user).to eq(user)

    end
end