Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users, :skip => [:registrations] 
  
  root to: "sale#index", as: :authenticated_root

  authenticated :user do
    resources :sale do
      post 'upload', action: :upload
    end
  end

  unauthenticated :user do
  end
end
