## Nuuvem App: Tutorial step by step to run this api

#####  System dependencies

- Docker (<https://www.docker.com/>)
- Docker-compose

#####  Gems

- gem 'devise' multi-client and secure token-based authentication for Rails.
- gem 'raiils-admin' simple, RailsAdmin is a Rails engine that provides an easy-to-use interface for managing your data.
- gem 'pundit' to build a simple, robust and scaleable authorization system
- gem 'brakeman' brakeman is a security scanner for Ruby on Rails applications.
- The following gems were used to help in the automated tests 
  - Rspec
  - ffaker
  - factory_bot_rails

#####  Step by step to run the project in localhost using docker-compose

1. Download the project from wetransfer

   ```bash
   git clone https://gitlab.com/vini.freire.oliveira/web-teste.git
   ```

2. Open the project folder

   ```bash
   cd web-teste
   ```

3. Build the containers and install the dependecies

   ```
   docker-compose run --rm app bundle install
   ```

4. Add jquery lib in project

   ```
   docker-compose run --rm app bundle exec yarn add jquery
   ```

5. Add bootstrap lib in project

   ```
   docker-compose run --rm app bundle exec yarn add bootstrap
   ```

6. Now, create database, generate migrations and run the seeds file:

   ```
   docker-compose run --rm app bundle exec rails db:create db:migrate db:seed
   ```

7. Start the server with

   ```
   docker-compose up
   ```

8. Open browser in Localhost:3000

   ```
   user:admin@nuuvem.com / password:nuuvem@123
   ```

#####  To run Rspec test results

1. To run the  service spec

   ```
   docker-compose run --rm app bundle exec rspec spec/requests/
   ```

2. To run all spec files

   ```
   docker-compose run --rm app bundle exec rspec spec
   ```

3. To run and save all spec files

   ```
   docker-compose run --rm app bundle exec rspec spec/ --format h > output_test.html
   ```

#####  To run Breakman report results

1. To generate the report

   ```
   docker-compose run --rm app bundle exec brakeman -f html >> report.html
   ```

###  Links

#####  [My Git](https://gitlab.com/vini.freire.oliveira) 

#####  [My LikedIn](https://www.linkedin.com/in/vinicius-freire-b53507107/) 

#####  The l6_http is a gem to make http and https request developed by me. Check on  GIT.

#####  Extra docker-compose commands

1. Start the container in shared folder

   ```bash
   docker-compose run --rm app bash
   ```

2. Start the ruby container in ruby console

   ```bash
   docker-compose run --rm app bundle exec rails c
   ```

