class UserPolicy < ApplicationPolicy
  def index?
    user.admin? || user.ordinary?
  end

  def show?
    user.admin? || user.ordinary?
  end

  def upload?
    user.admin?
  end

  def create?
    user.admin?
  end

  def destroy?
    user.admin?
  end
  
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
