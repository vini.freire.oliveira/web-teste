class SaleController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_user
  before_action :set_sale, only: [:show, :destroy]

  def index
    @sales = Sale.order("created_at DESC").all
    @items = Item.all
    @merchants = Merchant.all
    @purchasers = Purchaser.all
  end

  def show
  end

  def upload
    lines = sale_params[:tab].encode(Encoding::ISO_8859_1).split("\r\n")
    lines.each_with_index do |line, index|
      if index > 0
        tabs = line.split("\t");
        purchaser = set_purchaser(tabs[0],tabs[3])
        item = set_item(tabs[1],tabs[2])
        merchant = set_merchant(tabs[4],tabs[5])
        Sale.create(purchaser_id: purchaser.id, item_id: item.id, merchant_id: merchant.id)
      end
    end
    redirect_to action: "index"
  end

  def create
    @sale = Sale.new(sale_params)
    respond_to do |format|
      if @sale.save
        format.html { redirect_to action: "index", status: :found}
      else
        format.html { render action: "new"}
      end
    end
  end

  def destroy
    @sale.destroy
    respond_to do |format|
      format.html { redirect_to sale_url, status: :no_content }
    end
  end

  private

  def set_purchaser(name, count)
    Purchaser.create_with(count: count).find_or_create_by(name: name)
  end

  def set_item(description, price)
    Item.create(description:description, price: price)
  end

  def set_merchant(address,name)
    Merchant.create_with(address: address).find_or_create_by(name: name)
  end

  def set_sale
    @sale = Sale.find(params[:id])
  end

  def sale_params
    params.require(:sale).permit(:purchaser_id, :merchant_id, :tab, item_attributes:[:description, :price])
  end

  def authorize_user
    authorize User
  end

end
