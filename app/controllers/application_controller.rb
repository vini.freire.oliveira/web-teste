class ApplicationController < ActionController::Base
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
    include Pundit

    private

    def user_not_authorized
        render json: { message: "user not authorized" }.to_json, status: :unauthorized
    end
end
