class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable


  before_validation :set_user, on: :create

  enum user_type: [:admin, :ordinary]

  private

  def set_user
    self.user_type = :ordinary if User.all.count > 0
  end


end
