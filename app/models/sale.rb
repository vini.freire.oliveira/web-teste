class Sale < ApplicationRecord
  belongs_to :purchaser
  belongs_to :merchant
  belongs_to :item

  accepts_nested_attributes_for :item

end
