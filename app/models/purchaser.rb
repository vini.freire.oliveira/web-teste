class Purchaser < ApplicationRecord
  has_many :sales

  validates :name, presence: true, uniqueness: true
  validates :count, presence: true, :numericality =>{ :only_integer => true, :greater_than_or_equal_to => 1 }

end
