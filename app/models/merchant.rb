class Merchant < ApplicationRecord
  has_many :sales

  validates :name, presence: true, uniqueness: true
  validates :address, presence: true

end
