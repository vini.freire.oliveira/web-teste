$(document).ready(function () {
  checkApiSupport();
  $('#fileTab').change(function(){
    var file = this.files[0];
    readTextFile(file)
  });
});

// Check for the various File API support.
function checkApiSupport(){
  if (window.File && window.FileReader && window.FileList && window.Blob) {
    // Great success! All the File APIs are supported.
  } else {
    alert('The File APIs are not fully supported in this browser.');
  }
}

function readTextFile(file){
  var reader = new FileReader();
  reader.readAsBinaryString(file)

  reader.onload = function(e){
    $('#tab').val(reader.result);
  }

}